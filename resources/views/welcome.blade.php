@extends('layout.master')
@section('judul')
<h1>SELAMAT DATANG</h1><br>
@endsection
@section('content')

<p>Nama : {{$namaDepan}}</p>
<p>LastName : {{$namaBelakang}}</p>
<p>Jenis Kelamin: {{$gender}}</p>
<p>Bahasa : {{$languange}}</p>
<p>Kewarganegaraan : {{$nasionality}}</p>
<p>Biodata : {{$bio}}</p>

<b>Terima kasih telah Bergabung di Website Kami. Media Belajar kita bersama!</b>

@endsection